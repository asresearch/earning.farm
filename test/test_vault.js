const { BN, constants, expectEvent, expectRevert  } = require('openzeppelin-test-helpers');
const { expect  } = require('chai');
const getBlockNumber = require('./blockNumber')(web3)
var BigNumber = require('bignumber.js');
const { default: Big } = require('big.js');

const AddressArray = artifacts.require("AddressArray");

const EFCRVVault = artifacts.require("EFCRVVault");

const fs = require("fs");
const {DHelper, StepRecorder} = require("../util.js");
const { assert } = require('console');
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const SafeMath = artifacts.require("SafeMath");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20Token = artifacts.require("ERC20Token");
const TestERC20 = artifacts.require("TestERC20");

contract("TestGateKeeper2", (accounts) =>{
  sr = StepRecorder("ganache", "common");
  let ratio = 200000000
  let crv = {}
  let usdc = {}
  let e18 = "000000000000000000"
  let e6 = "000000"
  let ef = {}
  let vault = {}
  context("init", ()=>{
    it("init", async() =>{
      crv = await TestERC20.at(sr.value("crv"));
      await crv.generateTokens(accounts[0], "1000000000000000000000000000")
      await crv.generateTokens(accounts[1], "1000000000000000000000000000")
      usdc = await TestERC20.at(sr.value("usdc"));
      await usdc.generateTokens(accounts[0], "1000000000000000000000000000")
      await usdc.generateTokens(accounts[1], "1000000000000000000000000000")

      vault = await EFCRVVault.at(sr.value("vault"));

      await crv.approve(vault.address, "1000000000000000000000000000", {from:accounts[0]})
      await crv.approve(vault.address, "1000000000000000000000000000", {from:accounts[1]})
      await usdc.approve(vault.address, "1000000000000000000000000000", {from:accounts[0]})
      await usdc.approve(vault.address, "1000000000000000000000000000", {from:accounts[1]})

    });

    it("case", async() =>{
      //await vault.deposit("150000000" + e18)

      //await vault.deposit("75000000000000000000000000")

      //await vault.withdraw("17307692307692307000000000", false)

      data = web3.eth.abi.encodeFunctionCall({
        name: 'approve',
        type: 'function',
        inputs: [{"name": "spender", "type": "address"}, {"name": "amount", "type": "uint256"}]
        }, ["0xad3973f7Bb0b9FA6fb21757a7c81e254f30a322b", "10000000000000000000000000000000000000000000"]);
      console.log("data is:")
      console.log(data)
      //await vault.withdraw("15000000" + e18, false)  

      expect(1).to.equal(0)
    })
    it("vault", async() =>{
      /*await vault.deposit("10000" + e18)
      ef = await ERC20Token.at(sr.value("ef-token"))
      b0 = await ef.balanceOf(accounts[0])
      console.log("ef:", b0.toString())

      await vault.withdraw(b0, false)
      b0 = await crv.balanceOf(accounts[0])
      console.log("crv after withdraw:", b0.toString())

      await vault.depositStable("20000" + e6)
      await vault.deposit("10000" + e18, {from:accounts[1]})

      await vault.withdraw((await ef.balanceOf(accounts[0])), false)
      b0 = await crv.balanceOf(accounts[0])
      console.log("crv after withdraw:", b0.toString())

      await vault.withdraw((await ef.balanceOf(accounts[1])), true, {from:accounts[1]})
      b0 = await usdc.balanceOf(accounts[1])
      console.log("usdc after withdraw:", b0.toString())

      //expect(1).to.equal(0)
      await vault.depositStable("2000000" + e6)
      await vault.deposit("1000000" + e18, {from:accounts[1]})

      await vault.earnReward()*/
      
      console.log("virtual price:", (await vault.getPricePerEFToken()).toString())
    })

    
  })


})
