#!/usr/bin/python3
from brownie import EFCRVVault, config, network, Address, AddressArray,SafeERC20,TrustListFactory,SafeMath,TestERC20TokenFactory,TestERC20,USDT,DummyDexFactory,DummyDex,DummyDex2,Staker,EFCRVVault,Oracle,ERC20Token
from brownie.network.gas.strategies import GasNowStrategy

from scripts.helpful_scripts import (
    get_account,
    get_contract,
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
    BLOCK_CONFIRMATIONS_FOR_VERIFICATION
)
gas_strategy = "200 gwei"
def log(text, desc=''):
    print('\033[32m' + text + '\033[0m' + desc)


def deploy_address():
    account = get_account()
    address = Address.deploy({"from": account, "gas_price": gas_strategy,
                              "gas_limit": 3000000,
                              "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        address.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        Address.publish_source(address)
    else:
        address.tx.wait(1)
    print(f"Address  deployed to {address.address}")
    return address


def deploy_addressArray():
    account = get_account()
    address_array = AddressArray.deploy({"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        address_array.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        AddressArray.publish_source(address_array)
    else:
        address_array.tx.wait(1)
    print(f"Address  Array deployed to {address_array.address}")

    return address_array


def deploy_safeerc20():
    account = get_account()
    safeerc20 = SafeERC20.deploy({"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        safeerc20.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        SafeERC20.publish_source(safeerc20)
    else:
        safeerc20.tx.wait(1)
    print(f"SafeERC20  deployed to {safeerc20.address}")

    return safeerc20


def deploy_tl():
    account = get_account()
    trustlist_factory = TrustListFactory.deploy({"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        trustlist_factory.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        TrustListFactory.publish_source(trustlist_factory)
    else:
        trustlist_factory.tx.wait(1)
    print(f"TrustListFactory  deployed to {trustlist_factory.address}")

    return trustlist_factory

def deploy_safemath():
    account = get_account()
    safemath = SafeMath.deploy({"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})
    if config["networks"][network.show_active()].get("verify", False):
        safemath.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        SafeMath.publish_source(safemath)
    else:
        safemath.tx.wait(1)
    print(f"SafeMath  deployed to {safemath.address}")
    return safemath




def deploy_tt_factory():
    account = get_account()
    # token_factory = TestERC20TokenFactory.deploy({"from": account,
    #                                               "gas_price": gas_strategy,
    #                                               "gas_limit": 3000000,
    #                                      "allow_revert": True})

    # if config["networks"][network.show_active()].get("verify", False):
    #     token_factory.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
    #     TestERC20TokenFactory.publish_source(token_factory)
    # else:
    #     token_factory.tx.wait(1)
    # print(f"TestERC20TokenFactory  deployed to {token_factory.address}")

    token_factory = TestERC20TokenFactory.at("0x713F33E9a679B6a798Cf80FD73f9d24D1C9bd232")
    # usdc  =TestERC20.deploy("USDC", 6, "USDC",
    #                                   {"from": account,
    #                                               "gas_price": gas_strategy,
    #                                               "gas_limit": 3000000,
    #                                      "allow_revert": True});

    # if config["networks"][network.show_active()].get("verify", False):

    #     usdc.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)

    #     TestERC20.publish_source((usdc))
    # else:
    #     tx.wait(1)

    # print(f"usdc token  deployed to {usdc.address}")



    # crv =TestERC20.deploy("CRV", 18, "CRV",
    #                                   {"from": account,
    #                                               "gas_price": gas_strategy,
    #                                               "gas_limit": 3000000,
    #                                      "allow_revert": True});


    # if config["networks"][network.show_active()].get("verify", False):
    #     crv.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
    #     TestERC20.publish_source((crv))
    # else:
    #     tx.wait(1)

    # print(f"crv token  deployed to {crv.address}")




    # cvx =TestERC20.deploy("CVX", 18, "CVX",
    #                                   {"from": account,
    #                                               "gas_price": gas_strategy,
    #                                               "gas_limit": 3000000,
    #                                      "allow_revert": True});
    # if config["networks"][network.show_active()].get("verify", False):
    #     cvx.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
    #     TestERC20.publish_source(cvx)
    # else:
    #     tx.wait(1)
    # print(f"cvx token  deployed to {cvx.address}")

    # weth = TestERC20.deploy("WETH", 18,  "WETH",
    #                                   {"from": account,
    #                                               "gas_price": gas_strategy,
    #                                               "gas_limit": 3000000,
    #                                      "allow_revert": True});
    # if config["networks"][network.show_active()].get("verify", False):
    #     weth.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
    #     TestERC20.publish_source(weth)
    # else:
    #     tx.wait(1)

    # print(f"weth token  deployed to {weth.address}")




    cvxcrv = TestERC20.deploy("CVXCRV", 18, "CVXCRV",
                                      {"from": account,
                                                  "gas_price": gas_strategy,
                                                  "gas_limit": 3000000,
                                         "allow_revert": True});


    if config["networks"][network.show_active()].get("verify", False):
        cvxcrv.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        TestERC20.publish_source(cvxcrv)
    else:
        tx.wait(1)

    print(f"cvxcrv token  deployed to {cvxcrv.address}")


    crv_3 = TestERC20.deploy("3CRV", 18, "3CRV",
                                      {"from": account,
                                                  "gas_price": gas_strategy,
                                                  "gas_limit": 3000000,
                                         "allow_revert": True});


    if config["networks"][network.show_active()].get("verify", False):
        crv_3.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        TestERC20.publish_source(crv_3)
    else:
        tx.wait(1)

    print(f"3crv token  deployed to {crv_3.address}")





def deploy_usdt():
    account = get_account()
    usdt = USDT.deploy({"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        usdt.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        USDT.publish_source(usdt)
    else:
        usdt.tx.wait(1)
    print(f"USDT  deployed to {usdt.address}")

    return usdt

def deploy_oracle():
    account = get_account()
    oracle = Oracle.deploy({"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        oracle.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        Oracle.publish_source(oracle)
    else:
        usdt.tx.wait(1)
    print(f"oracle  deployed to {oracle.address}")

    return oracle



def deploy_dex(usdc,crv,cvx,weth,cvxcrv,crv_3,usdt):
    account = get_account()

    e = "00000000000000000000000000"
    t_0 = int("1" + e)
    t_1 = int("2000" + e)
    eth_crv = DummyDex.deploy(weth.address, crv.address,t_0,t_1,{"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        eth_crv.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        DummyDex.publish_source(eth_crv)
    else:
        dex_factory.tx.wait(1)
    print(f"eth_crv   deployed to {eth_crv.address}")

    t_0 = int("1" + e)
    t_1 = int("200" + e)
    eth_cvx = DummyDex.deploy(weth.address, cvx.address,t_0,t_1,{"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        eth_cvx.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        DummyDex.publish_source(eth_cvx)
    else:
        dex_factory.tx.wait(1)
    print(f"eth_cvx   deployed to {eth_cvx.address}")

    t_0 = int("1000000000000" + e)
    t_1 = int("4000" + e)
    eth_usdc = DummyDex.deploy(weth.address, usdc.address,t_0,t_1,{"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        eth_usdc.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        DummyDex.publish_source(eth_usdc)
    else:
        dex_factory.tx.wait(1)
    print(f"eth_usdc   deployed to {eth_usdc.address}")




    t_0 = int("4000" + e)
    t_1 = int("1000000000000" + e)
    eth_usdt = DummyDex.deploy(usdt.address, weth.address,t_0,t_1,{"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        eth_usdt.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        DummyDex.publish_source(eth_usdt)
    else:
        dex_factory.tx.wait(1)
    print(f"eth_usdt   deployed to {eth_usdt.address}")

    t_0 = int("1" + e)
    t_1 = int("1" + e)
    crv_cvxcrv = DummyDex2.deploy(crv.address, cvxcrv.address,t_0,t_1,{"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        crv_cvxcrv.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        DummyDex2.publish_source(crv_cvxcrv)
    else:
        dex_factory.tx.wait(1)
    print(f"crv_cvxcrv   deployed to {crv_cvxcrv.address}")


    staker = Staker.deploy(cvxcrv.address, crv.address, cvx.address,{"from": account, "gas_price": gas_strategy,
                                         "gas_limit": 1000000,
                                         "allow_revert": True})

    if config["networks"][network.show_active()].get("verify", False):
        staker.tx.wait(BLOCK_CONFIRMATIONS_FOR_VERIFICATION)
        Staker.publish_source(staker)
    else:
        dex_factory.tx.wait(1)

    print(f"staker   deployed to {staker.address}")


# @pytest.fixture(scope="session")
# def gett_oracle_tl(deploy_tl):
#     account = accounts[0]
#     tx =deploy_tl.createTrustList(['0x0000000000000000000000000000000000000000'],{"from": account});
#     tx.wait(1)
#     oracle_trustlist =  TrustList.at(tx.return_value);
#     return oracle_trustlist



def deploy_ef(deploy_addressArray,gett_token_tl):
    account = accounts[0]
    token_factory = ERC20TokenFactory.deploy({"from": account})
    token_factory.tx.wait(1)
    tx = token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "ef_curve_token", 18, "EF_CRV", True,{"from": account});
    tx.wait(1)
    ef_crv = ERC20Token.at(tx.return_value)
    ef_crv.changeTrustList(gett_token_tl.address,{"from": account});
    return ef_crv



def deploy_vault(deploy_ef,gett_token_tl,deploy_safemath):
    account = accounts[0]
    ef_vault = EFCRVVault.deploy(deploy_ef.address, {"from": account})
    ef_vault.tx.wait(1)
    tx = gett_token_tl.add_trusted(ef_vault.address,{"from": account})
    tx.wait(1)
    return ef_vault



def addExtraToken(deploy_vault):
    account = accounts[0]

    crv = "0xD533a949740bb3306d119CC777fa900bA034cd52";
    cvx = "0x4e3FBD56CD56c3e72c1403e103b45Db9da5B9D2B";
    eth_cvx = "0xb576491f1e6e5e62f1d8f26062ee822b40b0e0d4";
    tricrv = "0x6c3F90f043a72FA612cbac8115EE7e52BDe6E490";

    tripoolswap = "0xbEbc44782C7dB0a1A60Cb6fe97d0b483032FF1C7";

    tx = deploy_vault.addExtraToken(crv, crv, 0, {"from": account})
    tx.wait(1)
    tx = deploy_vault.addExtraToken(cvx, eth_cvx, 1, {"from": account})
    tx.wait(1)
    tx = deploy_vault.addExtraToken(tricrv, tripoolswap, 2, {"from": account})
    tx.wait(1)
    print("end of addExtraToken")






def crv_issue():
    account = get_account()
    print(f"On network {network.show_active()}")

    crv = get_contract("crv")
    print(crv.address)
    tx = crv.generateTokens("0x6c2da582218384dd956EB9ED49a892F2bA2D6340", 1000000000000000000000000000000000000000000000000000000000,
                            {"from": account, "gas_price": gas_strategy, "gas_limit": 1000000, "allow_revert": True})

    tx.wait(1)




def usdc_issue():
    account = get_account()
    print(f"On network {network.show_active()}")

    crv = get_contract("usdc")
    tx = crv.generateTokens(account.address, 100000000000000000000000,{"from": account, "gas_price": gas_strategy})
    tx.wait(1)
def deposit_crv_r():


    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")

    balance = usdc.balanceOf(account, {"from": account})

    tx = crv.approve(vault.address,1000000000000000000000000000000000000000, {"from": account, "gas_price": gas_strategy,"gas_limit": 1000000})
    tx.wait(1)


    tx = vault.deposit(3749997000000000000000000, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
def deposit_crv():
    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")

    balance = crv.balanceOf(account, {"from": account})

    tx = crv.approve(vault.address,balance+10000, {"from": account, "gas_price": gas_strategy,"gas_limit": 1000000})
    tx.wait(1)


    amount_balance = balance

    tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    tx = vault.deposit(amount_balance/4, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    tx = vault.deposit(amount_balance/2, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)

    balance = crv.balanceOf(account, {"from": account})
    log("balance of crv", str(balance))

    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance/5

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf after withdraw", str(balance))

    balance = crv.balanceOf(account, {"from": account})
    log("balance of crv after withdraw", str(balance))

def deposit_usdc():
    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")

    balance = usdc.balanceOf(account, {"from": account})

    tx = usdc.approve(vault.address,10000000000000000000000000000000, {"from": account, "gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)


    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.depositStable(balance/10, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)


    balance = usdc.balanceOf(account, {"from": account})
    log("balance of usdc", str(balance))

    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance/10

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy})
    tx.wait(1)

    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf after withdraw", str(balance))

    balance = usdc.balanceOf(account, {"from": account})
    log("balance of usdc after withdraw", str(balance))


def withdraw_usdc():
    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")



    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance

    tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy})
    # tx.wait(1)

    # balance = ef_token.balanceOf(account, {"from": account})
    # log("balance of enf after withdraw", str(balance))

    # balance = usdc.balanceOf(account, {"from": account})
    # log("balance of usdc after withdraw", str(balance))


def main():

    log("issue crv")
    #deploy_address()

    account = get_account()

    #print(f"Address  deployed to {address.address}")

    # #deploy_addressArray()       #
    # address_array = AddressArray.at("0x6b2E522EebB6d8F84Ec3C5b241fd603b3d8285E5")
    # AddressArray.publish_source(address_array)
    # print(f"Address  Array deployed to {address_array.address}")


    # #deploy_safeerc20()

    #     #deploy_tl()


    # #deploy_safemath()

    # safemath = SafeMath.at("0x0Ad3b8E3A32B5B2846CBF57FdD54D716ffa41018")
    # SafeMath.publish_source(safemath)
    # print(f"SafeMath  deployed to {safemath.address}")



    # # token_factory = TestERC20TokenFactory.at("0x713F33E9a679B6a798Cf80FD73f9d24D1C9bd232")
    # # print(f"TestERC20TokenFactory  deployed to {token_factory.address}")

    # usdc = TestERC20.at("0x588C12309DaE98970D1Cca34d73944dD3576666A")
    # TestERC20.publish_source(usdc)
    # print(f"usdc token  deployed to {usdc.address}")

    # crv = TestERC20.at("0xB0724732367C6330801B38B0a0268c890d6bA4dB")
    # TestERC20.publish_source(crv)
    # print(f"crv token  deployed to {crv.address}")

    # cvx = TestERC20.at("0x63267C6117Ac44ac6Af8c8952B7A2dfEe7815cB9")
    # TestERC20.publish_source(cvx)
    # print(f"cvx token  deployed to {cvx.address}")

    # weth = TestERC20.at("0xeBfFc1984Fa2B6bFd320671E3c468119635E6009")
    # TestERC20.publish_source(weth)
    # print(f"weth token  deployed to {weth.address}")

    # cvxcrv = TestERC20.at("0x6Fa43f562a29684C4F5034c8999206452f6f3Dd6")
    # TestERC20.publish_source(cvxcrv)
    # print(f"cvxcrv token  deployed to {cvxcrv.address}")

    # crv_3 = TestERC20.at("0x6FcDEa12409C5F2b78Fc926FaF67c94884e4249E")
    # TestERC20.publish_source(crv_3)
    # print(f"3crv token  deployed to {crv_3.address}")


    # #deploy_tt_factory()


    # #deploy_usdt()

    # usdt = USDT.at("0x4375DF54eB395206c5F7c26FC1544303375Be889")
    # TestERC20.publish_source(usdt)
    # print(f"usdt token  deployed to {usdt.address}")

    # eth_crv = DummyDex.at("0x6F600c18AABbeCbAc6dE27a744D9D5DE3fBf72b5")
    # DummyDex.publish_source(eth_crv)
    # print(f"eth_crv   deployed to {eth_crv.address}")

    # eth_cvx = DummyDex.at("0x712cFF3886676D22508b3E2921Cb2e6905F81619")
    # DummyDex.publish_source(eth_cvx)
    # print(f"eth_cvx   deployed to {eth_cvx.address}")

    # eth_usdc = DummyDex.at("0x92F4e7A76974C05629F3d69b47069E34ef98EA47")
    # DummyDex.publish_source(eth_usdc)
    # print(f"eth_usdc   deployed to {eth_usdc.address}")

    # eth_usdt = DummyDex.at("0xf959962EFF3A876e3c69AAEB345A4955E12759f0")
    # DummyDex.publish_source(eth_usdt)
    # print(f"eth_usdt   deployed to {eth_usdt.address}")

    # crv_cvxcrv = DummyDex2.at("0xf3415EF063E1A0bb54825578bba70CDacb2B37DF")
    # DummyDex2.publish_source(crv_cvxcrv)
    # print(f"crv_cvxcrv   deployed to {crv_cvxcrv.address}")

    # staker = Staker.at("0x2614859a0daF2Bb87D44195f39546B09152bcb70")
    # Staker.publish_source(staker)
    # print(f"staker   deployed to {staker.address}")

    # #deploy_dex(usdc,crv,cvx,weth,cvxcrv,crv_3,usdt)
    # # deploy_oracle()

    # oracle = Oracle.at("0x3A629aC3B00BfC6790826C2413379358cC873c66")
    # Oracle.publish_source(oracle)
    # print(f"oracle   deployed to {oracle.address}")




    # # addr = [crv.address, usdc.address,
    # #         eth_usdc.address, weth.address,
    # #         cvxcrv.address, eth_crv.address,
    # #         crv_cvxcrv.address, eth_usdt.address,
    # #         usdt.address, oracle.address, staker.address]

    # vault = EFCRVVault.at("0x5597ba3510A405C5F9Caa4C2241CF419dD5CC035")
    # EFCRVVault.publish_source(vault)
    # print(f"vault   deployed to {vault.address}")

    # ef_token = ERC20Token.at("0xA3886c44bd925c74cbC1cb7f9AceB21dcf129550")
    # ERC20Token.publish_source(ef_token)
    # print(f"ef_token   deployed to {ef_token.address}")


    # tx = vault.initAddresses(addr,{"from": account, "gas_price": gas_strategy,
    #                           "gas_limit": 3000000,
    #                           "allow_revert": True})
    # tx.wait(1)

    # tx = vault.addExtraToken(crv.address, crv.address, 0,

    #                          {"from": account, "gas_price": gas_strategy,
    #                           "gas_limit": 3000000,
    #                           "allow_revert": True})
    # tx.wait(1)

    # tx = vault.addExtraToken(cvx.address, eth_cvx.address, 1,{"from": account, "gas_price": gas_strategy,
    #                           "gas_limit": 3000000,
    #                           "allow_revert": True})
    # tx.wait(1)




    #crv_issue()

    # log("issue usdc")
    # usdc_issue()

    # log("deposit crv")
    #deposit_crv()





# log("deposit usdc")
#    deposit_usdc()


    # log("deposit usdc")
    #withdraw_usdc()
