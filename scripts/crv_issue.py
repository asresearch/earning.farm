#!/usr/bin/python3
from brownie import EFCRVVault, config, network
from brownie.network.gas.strategies import GasNowStrategy

from scripts.helpful_scripts import (
    get_account,
    get_contract,
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
)
gas_strategy = "200 gwei"
def log(text, desc=''):
    print('\033[32m' + text + '\033[0m' + desc)

def crv_issue(address):
    account = get_account()
    print(f"On network {network.show_active()}")

    crv = get_contract("crv")
    print(crv.address)
    tx = crv.generateTokens(address, 10000000000000000000000,
                            {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    tx.wait(1)



    # tx = crv.generateTokens("", 10000000000000000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    # tx = crv.generateTokens("0x75F7A0B820f7AaF559A0324192EF2F9fE85585D0", 10000000000000000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    # tx = crv.generateTokens("0xf8BAf7268F3daeFE4135F7711473aE8b6c3b47d8", 10000000000000000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    # tx = crv.generateTokens("0xC537A223b7Fe86483d31442248c5918177526BEf", 10000000000000000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    crv = get_contract("usdc")

    tx = crv.generateTokens(address, 10000000000,
                            {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    tx.wait(1)



    # tx = crv.generateTokens("0x59Fe8Cfd3F77E411e355Fa7Bdd70742515FC7F52", 10000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    # tx = crv.generateTokens("0x75F7A0B820f7AaF559A0324192EF2F9fE85585D0", 10000000000,1
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    # tx = crv.generateTokens("0xf8BAf7268F3daeFE4135F7711473aE8b6c3b47d8", 10000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)


    # tx = crv.generateTokens("0xC537A223b7Fe86483d31442248c5918177526BEf", 10000000000,
    #                         {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})

    # tx.wait(1)



def usdc_issue():
    account = get_account()
    print(f"On network {network.show_active()}")
    crv = get_contract("usdc")

    tx = crv.generateTokens(account.address, 10000000000,{"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})
    tx.wait(1)



def deposit_crv_r():

    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")

    balance = crv.balanceOf(account, {"from": account})

    tx = crv.approve(vault.address,1000000000000000000000000000000000000000, {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True}
)
    tx.wait(1)

    tx = vault.deposit(balance, {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})
    tx.wait(1)



    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)




def deposit_crv():
    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")
    balance = crv.balanceOf(account, {"from": account})

    tx = crv.approve(vault.address, 0, {"from": account, "gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)

    tx = crv.approve(vault.address, balance, {"from": account, "gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)

    tx = vault.deposit(balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)
    # tx = vault.deposit(amount_balance/4, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance/2, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.deposit(amount_balance, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)

    balance = crv.balanceOf(account, {"from": account})
    log("balance of crv", str(balance))

    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance/5

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)

    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)

    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    # tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    # tx.wait(1)
    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf after withdraw", str(balance))

    balance = crv.balanceOf(account, {"from": account})
    log("balance of crv after withdraw", str(balance))


    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance/5

    tx = vault.withdraw(withdraw_amount, False, {"from": account,"gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)


def deposit_usdc():
    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")
    balance = usdc.balanceOf(account, {"from": account})
    # tx = usdc.approve(vault,10000000000000000000000000000000, {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})
    # tx.wait(1)
    total_in_stable = vault.getTotalVolumeInStable()
    log("before deposit total in stable", str(total_in_stable))
    tx = vault.depositStable(10000000000, {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})
    tx.wait(1)
    total_in_stable = vault.getTotalVolumeInStable()
    log("after deposit total in stable", str(total_in_stable))


    # balance = usdc.balanceOf(account, {"from": account})
    # log("balance of usdc", str(balance))

    # balance = ef_token.balanceOf(account, {"from": account})
    # log("balance of enf", str(balance))

    # withdraw_amount = balance/5

    # tx = vault.withdraw(withdraw_amount, True, {"from": account, "gas_price": gas_strategy, "gas_limit": 3000000, "allow_revert": True})
    # tx.wait(1)


    # balance = ef_token.balanceOf(account, {"from": account})
    # log("balance of enf after withdraw", str(balance))

    # balance = usdc.balanceOf(account, {"from": account})
    # log("balance of usdc after withdraw", str(balance))


def withdraw_usdc():
    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")



    balance = ef_token.balanceOf(account, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance

    tx = vault.withdraw(withdraw_amount, True, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)

"""
"""
def destroy_token(addr):

    account = get_account()
    crv = get_contract("crv")
    usdc = get_contract("usdc")
    vault = get_contract("vault")
    ef_token = get_contract("ef-token")



    balance = ef_token.balanceOf(addr, {"from": account})
    log("balance of enf", str(balance))

    withdraw_amount = balance

    tx = ef_token.destroyTokens(addr,withdraw_amount, {"from": account,"gas_price": gas_strategy,"gas_limit": 1000000,"allow_revert": True})
    tx.wait(1)

def earnReward():

    account = get_account()

    vault = get_contract("vault")

    tx = vault.earnReward( {"from": account,"gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)


def changeFeePool(address):

    account = get_account()

    vault = get_contract("vault")

    tx = vault.changeFeePool(address, {"from": account,"gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)

def changeHarvestFee(fee):

    account = get_account()

    vault = get_contract("vault")

    tx = vault.changeHarvestFee( fee,{"from": account,"gas_price": gas_strategy,"gas_limit": 3000000,"allow_revert": True})
    tx.wait(1)


def main():
    #destroy_token("0x6c2da582218384dd956EB9ED49a892F2bA2D6340")

#     log("issue crv")

#    crv_issue()

#     # log("issue usdc")
#     usdc_issue()
#     deposit_crv_r()
#

#     # log("deposit crv")
#     #
    earnReward()
    #crv_issue("0xe72979D7f270c17B50666A8E79AC88B43e121B0a")
    # deposit_crv()
#    deposit_usdc()
    # changeFeePool("0x6c2da582218384dd956EB9ED49a892F2bA2D6340")
    # changeHarvestFee(1000)





# # log("deposit usdc")
# #    deposit_usdc()


#     # log("deposit usdc")
#     withdraw_usdc()
